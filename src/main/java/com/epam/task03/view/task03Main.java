package com.epam.task03.view;

import com.epam.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class task03Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void startTask03() {
        final int limit = 10;
        final int start = 0;
        final int end = 100;
        List<Integer> intList =
                new Random()
                        .ints(limit, start, end)
                        .boxed()
                        .sorted()
                        .collect(Collectors.toList());

        logger.info("Your list: " + intList + "\n");

        Stream<Integer> myStream = intList.stream();

        Optional<Integer> max = intList.stream()
                .max(Integer::compareTo);
        logger.info("Max value: " + max + "\n");

        Optional<Integer> sumOne = intList.stream()
                .reduce((a, b) -> a + b);

        logger.info("Sum with reduce: " + sumOne + "\n");

        int sumTwo = intList.stream()
                .mapToInt((a) -> a)
                .sum();

        logger.info("Sum with .sum: " + sumTwo + "\n");

        String stats = intList.stream()
                .mapToInt((a) -> a)
                .summaryStatistics()
                .toString();
        logger.info("Summary statistics: " + stats + "\n");

        OptionalDouble averageOp = intList.stream()
                .mapToInt((a) -> a)
                .average();
        double average = averageOp.isPresent() ? averageOp.getAsDouble() : 0;
        List<Integer> biggerThanAv = myStream
                .filter(a -> a > average)
                .collect(Collectors.toList());
        logger.info("Numbers(count = " + biggerThanAv.size() + ") that are bigger than average: " + biggerThanAv + "\n");
    }
}
