package com.epam;

@FunctionalInterface
public interface Application {
    void start();
}
