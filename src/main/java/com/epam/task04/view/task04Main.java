package com.epam.task04.view;

import com.epam.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class task04Main {
    private static final Scanner scanner = new Scanner(System.in);

    private static Logger logger = LogManager.getLogger(Main.class);

    private static List<String> getWords(String text) {
        List<String> list = new ArrayList<>();

        Pattern p = Pattern.compile("[\\w']+");
        Matcher m = p.matcher(text);
        while (m.find()) {
            list.add(text.substring(m.start(), m.end()));
        }
        return list;
    }

    public static void startTask04() {
        logger.info("Input your text:");
        String text = scanner.nextLine();
        List<String> listStr = getWords(text);

        Stream<String> myStream = listStr.stream();

        logger.info("Number of unique words: \n"
                + listStr.stream()
                .map(String::toLowerCase)
                .distinct()
                .collect(Collectors.toList())
                .size()
        );

        logger.info("Sorted list of all unique words: \n"
                + myStream
                .map(String::toLowerCase)
                .distinct()
                .sorted()
                .collect(Collectors.toList())
        );
        Map<String, Long> countWords = listStr.stream()
                .map(String::toLowerCase)
                .collect(
                        Collectors.groupingBy(String::toString, Collectors.counting())
                );
        logger.info("Unique words(first version): \n" + countWords.toString());

        logger.info("Unique words(second version): {\n");
        listStr.stream()
                .map(String::toLowerCase)
                .collect(
                        Collectors.groupingBy(String::toString, Collectors.counting())
                )
                .forEach((a, b) -> {
                            if (b == 1) {
                                System.out.print(a + "=" + b + "  ");
                            }
                        }
                );
        logger.info("}");
    }
}
