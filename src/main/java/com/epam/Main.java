package com.epam;


import com.epam.task02.model.methodReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;


public class Main {
    private Map<String, String> menu;
    private Map<String, Application> methodsMenu;
    private static Logger logger = LogManager.getLogger(Main.class);

    private static final Scanner scanner = new Scanner(System.in);

    private Main() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - start Priority Queue Task");
        menu.put("2", " 2 - start Priority Queue Task");
        menu.put("3", " 3 - start Priority Queue Task");
        menu.put("4", " 4 - start Priority Queue Task");
        menu.put("5", " 5 - Exit\n");

        methodReference menu;
        methodsMenu.put("1", com.epam.task01.view.task01Main::startTask01);
        methodsMenu.put("2", com.epam.task02.view.task02Main::main);
        methodsMenu.put("3", com.epam.task03.view.task03Main::startTask03);
        methodsMenu.put("4", com.epam.task04.view.task04Main::startTask04);


    }

    private void showMenu() {
        for (String value : menu.values()) {
            logger.trace("\n" + value);
        }
    }

    public static void main(String[] args) {

        Main main = new Main();
        String keyMenu;

        do {
            main.showMenu();
            logger.trace("Select menu point: \n");
            keyMenu = scanner.nextLine();
            main.methodsMenu.get(keyMenu).start();
        } while (!keyMenu.equals("4"));
    }
}