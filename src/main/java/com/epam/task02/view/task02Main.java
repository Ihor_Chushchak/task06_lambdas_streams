package com.epam.task02.view;

import com.epam.Application;
import com.epam.task02.model.methodReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class task02Main {

    private Map<String, String> menu;
    private Map<String, Application> methodsMenu;
    private static Logger logger = LogManager.getLogger(com.epam.Main.class);

    private static final Scanner scanner = new Scanner(System.in);

    private task02Main() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - start as lambda");
        menu.put("2", " 2 - start as method reference");
        menu.put("3", " 3 - start as anonymous class");
        menu.put("4", " 4 - start as object of command class");
        menu.put("5", " 5 - Exit\n");

        methodsMenu.put("1", com.epam.task02.model.lambda::invokeAsLambda);
        methodsMenu.put("2", new methodReference()::invokeAsMethodReference);
        methodsMenu.put("3", com.epam.task02.model.anonymousClass::invokeAsAnonymousClass);
        methodsMenu.put("4", com.epam.task02.model.objectOfCommandClass::invokeAsObject);
    }

    private void showMenu() {
        for (String value : menu.values()) {
            logger.trace("\n" + value);
        }
    }

    public static void main() {

        com.epam.task02.view.task02Main main = new com.epam.task02.view.task02Main();
        String keyMenu;

        do {
            main.showMenu();
            logger.trace("Select menu point: \n");
            keyMenu = scanner.nextLine();
            main.methodsMenu.get(keyMenu).start();
        } while (!keyMenu.equals("5"));
    }
}
