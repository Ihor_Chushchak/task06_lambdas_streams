package com.epam.task02.model;

import com.epam.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class objectOfCommandClass implements Command {
    private static Logger logger = LogManager.getLogger(Main.class);
    private static Scanner scanner = new Scanner(System.in);

    public void execute(String line) {
        logger.info(line.replaceAll("[a-z]*[A-Z]*", "#"));
    }

    public static void invokeAsObject() {
        Command command = new objectOfCommandClass();
        logger.info("Enter text line:");
        String line = scanner.nextLine();
        command.execute(line);

    }
}

