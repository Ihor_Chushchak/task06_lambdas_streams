package com.epam.task02.model;

import com.epam.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class anonymousClass {
    private static Logger logger = LogManager.getLogger(Main.class);

    private static Scanner scanner = new Scanner(System.in);

    public static void invokeAsAnonymousClass() {
        logger.info("Enter text line: ");
        String line = scanner.nextLine();
        Command command = new Command() {
            @Override
            public void execute(String line) {
                String[] array = new String[line.length()];
                String symbol;
                for (int i = 0; i < array.length; i++) {
                    symbol = String.valueOf(line.charAt(i));
                    if (i % 2 == 0) {
                        array[i] = symbol.toUpperCase();
                    } else {
                        array[i] = symbol.replaceAll(".", "<^>");
                    }
                }
                logger.info("Executed text: ");
                for (String i : array) {
                    logger.info(i);
                }
            }
        };
        command.execute(line);

    }
}
