package com.epam.task02.model;

@FunctionalInterface
public interface Command {
    void execute(String line);
}
