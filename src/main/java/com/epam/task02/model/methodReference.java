package com.epam.task02.model;

import com.epam.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class methodReference {
    private static Logger logger = LogManager.getLogger(Main.class);
    private static Scanner scanner = new Scanner(System.in);

    private void execute(String line) {
        logger.info("Text to lower case: " + line.toLowerCase());
    }

    public methodReference() {
    }

    public void invokeAsMethodReference() {
        Command command = line1 -> execute(line1);
        logger.info("Enter text line:");
        String line = scanner.nextLine();
        command.execute(line);
    }
}
