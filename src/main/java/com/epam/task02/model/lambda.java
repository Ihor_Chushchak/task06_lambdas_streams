package com.epam.task02.model;

import com.epam.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class lambda {
    private static Logger logger = LogManager.getLogger(Main.class);

    private static Scanner scanner = new Scanner(System.in);

    public static void invokeAsLambda() {
        Command execute = string -> logger.info("Text to upper case: " + string.toUpperCase());
        logger.info("Enter text line:");
        String line = scanner.nextLine();
        execute.execute(line);
    }
}
