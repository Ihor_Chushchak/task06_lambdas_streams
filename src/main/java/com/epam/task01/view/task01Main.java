package com.epam.task01.view;

import com.epam.Main;
import com.epam.task01.model.FuncInterfaceItself;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class task01Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    private static int getMax(int a, int b, int c) {
        int[] array = {a, b, c};
        int maxNumber = a;
        for (int i = 0; i < array.length; i++) {
            if (maxNumber < array[i]) {
                maxNumber = array[i];
            }
        }
        return maxNumber;
    }

    public static void startTask01() {
        Scanner scanner = new Scanner(System.in);
        logger.info("Enter first variable: ");
        int firstVariable = scanner.nextInt();
        logger.info("Enter second variable: ");
        int secondVariable = scanner.nextInt();
        logger.info("Enter third variable: ");
        int thirdVariable = scanner.nextInt();
        logger.info("Your numbers:\n" + firstVariable + "\n" + secondVariable + "\n" + thirdVariable);
        FuncInterfaceItself methodOne = (int a, int b, int c) -> getMax(a, b, c);
        logger.info("Max number is: " + methodOne.method(firstVariable, secondVariable, thirdVariable));
        FuncInterfaceItself methodTwo = (int a, int b, int c) -> ((a + b + c) / 3) * 100;
        logger.info(("Average is: " + methodTwo.method(firstVariable, secondVariable, thirdVariable) / 100));
    }
}
